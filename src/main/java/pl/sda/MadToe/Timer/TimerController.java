package pl.sda.MadToe.Timer;


import com.sun.org.apache.xpath.internal.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.sda.MadToe.Timer.Beans.ILog;
import pl.sda.MadToe.Timer.Beans.Task;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller
public class TimerController {

    @Autowired
    ILog log;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String timer(ModelMap model) {
        System.out.println("wypluwam");
        return "timer";
    }


    @GetMapping("/start")
    public String start(@RequestParam("name") String name, ModelMap model) {
        Task task = log.getTask(name);
        if (null == task) task = new Task();
        task.setStart(LocalDateTime.now());
        task.setName(name);
        log.addToList(task);
        return "timer";
    }

    @GetMapping("/stop")
    public String stop(@RequestParam("name") String name, ModelMap model) {
        Task task = log.getTask(name);
        if (null == task) task = new Task();
        task.setFinish(LocalDateTime.now());
        task.setName(name);
        return "timer";
    }

    @GetMapping("/logs")
    public String logs(ModelMap modelMap) {
        List<Task> taskList = log.getTaskList();
        List<String> tolist = new ArrayList<>();
        for (Task task: taskList) {
            StringBuilder s = new StringBuilder();
            s.append(task.getName());
            s.append(": start: ");
            s.append(task.getStart());
            s.append(": finish: ");
            s.append(task.getFinish());
            tolist.add(s.toString());
        }
        modelMap.addAttribute("logs", tolist);
        return "logs";
    }
}
