package pl.sda.MadToe.Timer.Beans;

import java.util.List;

public interface ILog {

    List<Task> getTaskList();

    void addToList(Task task);

    Task getTask(String name);
}
