package pl.sda.MadToe.Timer.Beans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Scope("singleton")
public class Log implements ILog{

    private List<Task> tasklist;

    @Override
    public List<Task> getTaskList() {
        return tasklist;
    }

    @Override
    public void addToList(Task task) {
        tasklist.add(task);
    }

    @Override
    public Task getTask(String name) {
        for (Task t : tasklist) {
            if(t.getName().equals(name)) return t;
        }
        return null;
    }
}
